if ($host.Name -eq 'ConsoleHost')
{
  Invoke-Expression (&starship init powershell)

  Import-Module posh-git

  $env:POSH_GIT_ENABLED = $true
  $env:POSH_AZURE_ENABLED = $true
  . ~/Documents/PowerShell/Microsoft.PSReadline_profile.ps1
  . ~/Documents/PowerShell/chezmoi.completion_profile.ps1

}

function PS_ll
{
  & gci -Force
}

function PS_ls
{
  & gci -Force
}

function PS_home
{
  Set-Location -Path $HOME
}

function PS_scbp
{
  scb $pwd.Path
}

function PS_repos
{
  Set-Location -Path D:/
}

function PS_up_one_directory
{
  $source = Get-Location
  $destination = Split-Path -Path $source -Parent
  Set-Location -Path $destination
  PS_ll
}

function PS_up_two_directories
{
  for ($i = 0; $i -lt 1; $i++)
  {
    & PS_up_one_directory
  }
}

function PS_up_three_directories
{
  for ($i = 0; $i -lt 2; $i++)
  {
    & PS_up_one_directory
  }
}

function PS_up_four_directories
{
  for ($i = 0; $i -lt 3; $i++)
  {
    & PS_up_one_directory
  }
}

function PS_up_five_directories
{
  for ($i = 0; $i -lt 4; $i++)
  {
    & PS_up_one_directory
  }
}

Set-Alias -Name repos -Value PS_repos
Set-Alias -Name vim -Value 'nvim'
Set-Alias -Name lg -Value 'lazygit'
Set-Alias -Name scbp -Value PS_scbp
Set-Alias -Name "la" -Value PS_ll
Set-Alias -Name "ls" -Value PS_ls
Set-Alias -Name "~" -Value PS_home
Set-Alias -Name ".." -Value PS_up_one_directory
Set-Alias -Name "..." -Value PS_up_two_directories
Set-Alias -Name "...." -Value PS_up_three_directories
Set-Alias -Name "....." -Value PS_up_four_directories
Set-Alias -Name "......" -Value PS_up_five_directories
