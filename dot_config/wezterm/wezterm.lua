-- Pull in the wezterm API
local wezterm = require("wezterm")
-- local launcher = require("launcher")

-- This table will hold the configuration.
local config = {}

-- In newer versions of wezterm, use the config_builder which will
-- help provide clearer error messages
if wezterm.config_builder then
	config = wezterm.config_builder()
end

-- This is where you actually apply your config choices

-- For example, changing the color scheme:
config.color_scheme = "GruvboxDark"
config.font = wezterm.font("Hack Nerd Font Propo")
config.default_prog = { "pwsh", "-l" }
config.default_cwd = "d:/"

config.keys = {
	{ key = "l", mods = "ALT", action = wezterm.action.ShowLauncher },
	{ key = "<", mods = "CTRL", action = wezterm.action.DecreaseFontSize },
	{ key = ">", mods = "CTRL", action = wezterm.action.IncreaseFontSize },
}

local launch_menu = {}

if wezterm.target_triple == "x86_64-pc-windows-msvc" then
	table.insert(launch_menu, {
		label = "PowerShell",
		args = { "pwsh.exe", "-NoLogo" },
	})

	table.insert(launch_menu, {
		label = "Ubuntu",
		args = { "ubuntu.exe" },
	})

	table.insert(launch_menu, {
		label = "Command Prompt",
		args = { "cmd.exe" },
	})
end

config.launch_menu = launch_menu

-- and finally, return the configuration to wezterm
return config
