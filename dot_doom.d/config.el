;;; $DOOMDIR/config.el -*- lexical-binding: t; -*-

;; Place your private configuration here! Remember, you do not need to run 'doom
;; sync' after modifying this file!


;; Some functionality uses this to identify you, e.g. GPG configuration, email
;; clients, file templates and snippets. It is optional.
(setq user-full-name "Stephen Walsh"
      user-mail-address "stephen@connectwithawalsh.com")

;; Doom exposes five (optional) variables for controlling fonts in Doom:
;;
;; - `doom-font' -- the primary font to use
;; - `doom-variable-pitch-font' -- a non-monospace font (where applicable)
;; - `doom-big-font' -- used for `doom-big-font-mode'; use this for
;;   presentations or streaming.
;; - `doom-unicode-font' -- for unicode glyphs
;; - `doom-serif-font' -- for the `fixed-pitch-serif' face
;;
;; See 'C-h v doom-font' for documentation and more examples of what they
;; accept. For example:
;;
;;(setq doom-font (font-spec :family "Fira Code" :size 12 :weight 'semi-light)
;;      doom-variable-pitch-font (font-spec :family "Fira Sans" :size 13))

(setq doom-font (font-spec :family "Hack Nerd Font Propo" :size 18 :weight 'regular)
      doom-variable-pitch-font (font-spec :family "Hack Nerd Font Propo" :size 16)
      doom-unicode-font (font-spec :family "Hack Nerd Font Propo")
      doom-big-font (font-spec :family "Hack Nerd Font Propo" :size 24))

;; If you or Emacs can't find your font, use 'M-x describe-font' to look them
;; up, `M-x eval-region' to execute elisp code, and 'M-x doom/reload-font' to
;; refresh your font settings. If Emacs still can't find your font, it likely
;; wasn't installed correctly. Font issues are rarely Doom issues!

;; There are two ways to load a theme. Both assume the theme is installed and
;; available. You can either set `doom-theme' or manually load a theme with the
;; `load-theme' function. This is the default:
(setq doom-theme 'doom-gruvbox)

;; This determines the style of line numbers in effect. If set to `nil', line
;; numbers are disabled. For relative line numbers, set this to `relative'.
(setq display-line-numbers-type 'relative)

;; If you use `org' and don't want your org files in the default location below,
;; change `org-directory'. It must be set before org loads!
(setq org-directory "~/iCloudDrive/iCloud~com~appsonthemove~beorg/org")
(global-set-key (kbd "<f12>") 'org-agenda)
(global-set-key (kbd "<f5>") 'bh/org-todo)
(global-set-key (kbd "<S-f5>") 'bh/widen)

;; Whenever you reconfigure a package, make sure to wrap your config in an
;; `after!' block, otherwise Doom's defaults may override your settings. E.g.
;;
;;   (after! PACKAGE
;;     (setq x y))
;;

;;
;;   - Setting file/directory variables (like `org-directory')
;;   - Setting variables which explicitly tell you to set them before their
;;     package is loaded (see 'C-h v VARIABLE' to look up their documentation).
;;   - Setting doom variables (which start with 'doom-' or '+').
;;
;; Here are some additional functions/macros that will help you configure Doom.
;;
;; - `load!' for loading external *.el files relative to this one
;; - `use-package!' for configuring packages
;; - `after!' for running code after a package has loaded
;; - `add-load-path!' for adding directories to the `load-path', relative to
;;   this file. Emacs searches the `load-path' when you load packages with
;;   `require' or `use-package'.
;; - `map!' for binding new keys
;;
;; To get information about any of these functions/macros, move the cursor over
;; the highlighted symbol at press 'K' (non-evil users must press 'C-c c k').
;; This will open documentation for it, including demos of how they are used.
;; Alternatively, use `C-h o' to look up a symbol (functions, variables, faces,
;; etc).
;;
;; You can also try 'gd' (or 'C-c c d') to jump to their definition and see how
;; they are implemented.

(after! org

  ;; Autosave four times per hour
  (run-at-time "00:14" 3600 'org-save-all-org-buffers)
  (run-at-time "00:29" 3600 'org-save-all-org-buffers)
  (run-at-time "00:44" 3600 'org-save-all-org-buffers)
  (run-at-time "00:59" 3600 'org-save-all-org-buffers)

  (map! :n "M-j" #'org-metadown
        :n "M-k" #'org-metaup)
  (setq org-default-notes-file "~/iCloudDrive/iCloud~com~appsonthemove~beorg/org/resources.org")
  (setq +org-capture-journal-file "~/iCloudDrive/iCloud~com~appsonthemove~beorg/org/refile.org")
  (setq +org-capture-todo-file "~/iCloudDrive/iCloud~com~appsonthemove~beorg/org/refile.org")
  (add-to-list 'org-modules
               'org-habit
               )

  ;;
  ;; Tasks & States
  ;;

  (setq org-log-done (quote time))
  (setq org-log-into-drawer t)
  (setq org-log-state-notes-insert-after-drawers nil)
  (setq org-deadline-warning-days 10)

  (setq org-todo-keywords
        (quote ((sequence "TODO(t)" "NEXT(n)" "|" "DONE(d)")
                (sequence "WAITING(w@/!)" "HOLD(h@/!)" "|" "CANCELLED(c@/!)" "PHONE" "MEETING"))))

  (setq org-todo-keyword-faces
        (quote (("TODO" :foreground "PaleVioletRed1" :weight bold)
                ("NEXT" :foreground "CornflowerBlue" :weight bold)
                ("DONE" :foreground "MediumTurquoise" :weight bold)
                ("WAITING" :foreground "goldenrod" :weight bold)
                ("HOLD" :foreground "MediumPurple" :weight bold)
                ("CANCELLED" :foreground "MediumTurquoise" :weight bold)
                ("MEETING" :foreground "MediumTurquoise" :weight bold)
                ("PHONE" :foreground "MediumTurquoise" :weight bold))))

  (setq org-todo-state-tags-triggers
        (quote (("CANCELLED" ("CANCELLED" . t))
                ("WAITING" ("WAITING" . t))
                ("HOLD" ("WAITING") ("HOLD" . t))
                (done ("WAITING") ("HOLD"))
                ("TODO" ("WAITING") ("CANCELLED") ("HOLD"))
                ("NEXT" ("WAITING") ("CANCELLED") ("HOLD"))
                ("DONE" ("WAITING") ("CANCELLED") ("HOLD")))))

  ;; Tags with fast selection keys
  (setq org-tag-alist (quote ((:startgroup)
                              ("@errand" . ?e)
                              ("@office" . ?o)
                              ("@home" . ?H)
                              (:endgroup)
                              ("WAITING" . ?w)
                              ("HOLD" . ?h)
                              ("PERSONAL" . ?P)
                              ("WORK" . ?W)
                              ("ORG" . ?O)
                              ("crypt" . ?E)
                              ("NOTE" . ?n)
                              ("CANCELLED" . ?c)
                              ("FLAGGED" . ??))))

  ;;
  ;; Capture templates for: TODO tasks, Notes, appointments, phone calls, meetings, and org-protocol
  ;;

  (setq org-capture-templates
        (quote (("t" "Todo" entry (file +org-capture-todo-file)
                 "* TODO %?\n%U\n%a\n" :clock-in t :clock-resume t)
                ("r" "Respond" entry (file +org-capture-todo-file)
                 "* NEXT Respond to %:from on %:subject\nSCHEDULED: %t\n%U\n%a\n" :clock-in t :clock-resume t :immediate-finish t)
                ("n" "Note" entry (file org-default-notes-file)
                 "* %? :NOTE:\n%U\n%a\n" :clock-in t :clock-resume t)
                ("j" "Journal" entry (file+datetree +org-capture-todo-file)
                 "* %?\n%U\n" :clock-in t :clock-resume t)
                ("w" "org-protocol" entry (file +org-capture-todo-file)
                 "* TODO Review %c\n%U\n" :immediate-finish t)
                ("m" "Meeting" entry (file +org-capture-todo-file)
                 "* MEETING with %? :MEETING:\n%U" :clock-in t :clock-resume t)
                ("p" "Phone call" entry (file +org-capture-todo-file)
                 "* PHONE %? :PHONE:\n%U" :clock-in t :clock-resume t)
                ("h" "Habit" entry (file +org-capture-todo-file)
                 "* NEXT %?\n%U\n%a\nSCHEDULED: %(format-time-string \"%<<%Y-%m-%d %a .+1d/3d>>\")\n:PROPERTIES:\n:STYLE: habit\n:REPEAT_TO_STATE: NEXT\n:END:\n"))))

  ;;
  ;;Refile Setup
  ;;

  ;; Targets include this file and any file contributing to the agenda - up to 9 levels deep
  (setq org-refile-targets (quote ((nil :maxlevel . 9)
                                   (org-agenda-files :maxlevel . 9))))

  ;; Use full outline paths for refile targets - we file directly with IDO
  (setq org-refile-use-outline-path t)

  ;; Allow refile to create parent tasks with confirmation
  (setq org-refile-allow-creating-parent-nodes (quote confirm))

  ;;
  (advice-add 'org-refile :after 'org-save-all-org-buffers)

  ;;
  ;; Clocking
  ;;

  ;; Remove empty LOGBOOK drawers on clock out
  (defun bh/remove-empty-drawer-on-clock-out ()
    (interactive)
    (save-excursion
      (beginning-of-line 0)
      (org-remove-empty-drawer-at (point))))

  (add-hook 'org-clock-out-hook 'bh/remove-empty-drawer-on-clock-out 'append)

  ;; Resume clocking task when emacs is restarted
  (org-clock-persistence-insinuate)
  ;; Show lot of clocking history so it's easy to pick items off the C-F11 list
  (setq org-clock-history-length 23)
  ;; Resume clocking task on clock-in if the clock is open
  (setq org-clock-in-resume t)
  ;; Change tasks to NEXT when clocking in
  (setq org-clock-in-switch-to-state 'bh/clock-in-to-next)
  ;; Separate drawers for clocking and logs
  (setq org-drawers (quote ("PROPERTIES" "LOGBOOK")))
  ;; Save clock data and state changes and notes in the LOGBOOK drawer
  (setq org-clock-into-drawer t)
  ;; Sometimes I change tasks I'm clocking quickly - this removes clocked tasks with 0:00 duration
  (setq org-clock-out-remove-zero-time-clocks t)
  ;; Clock out when moving task to a done state
  (setq org-clock-out-when-done t)
  ;; Save the running clock and all clock history when exiting Emacs, load it on startup
  (setq org-clock-persist t)
  ;; Do not prompt to resume an active clock
  (setq org-clock-persist-query-resume nil)
  ;; Enable auto clock resolution for finding open clocks
  (setq org-clock-auto-clock-resolution (quote when-no-clock-is-running))
  ;; Include current clocking task in clock reports
  (setq org-clock-report-include-clocking-task t)

  (defun bh/clock-in-to-next (kw)
    ;; Switch a task from TODO to NEXT when clocking in.
    ;; Skips capture tasks, projects, and subprojects.
    ;; Switch projects and subprojects from NEXT back to TODO
    (when (not (and (boundp 'org-capture-mode) org-capture-mode))
      (cond
       ((and (member (org-get-todo-state) (list "TODO"))
             (bh/is-task-p))
        "NEXT")
       ((and (member (org-get-todo-state) (list "NEXT"))
             (bh/is-project-p))
        "TODO"))))

  (defun bh/is-task-p ()
    "Any task with a todo keyword and no subtask"
    (save-restriction
      (widen)
      (let ((has-subtask)
            (subtree-end (save-excursion (org-end-of-subtree t)))
            (is-a-task (member (nth 2 (org-heading-components)) org-todo-keywords-1)))
        (save-excursion
          (forward-line 1)
          (while (and (not has-subtask)
                      (< (point) subtree-end)
                      (re-search-forward "^\*+ " subtree-end t))
            (when (member (org-get-todo-state) org-todo-keywords-1)
              (setq has-subtask t))))
        (and is-a-task (not has-subtask)))))

  (defun bh/is-project-p ()
    "Any task with a todo keyword subtask"
    (save-restriction
      (widen)
      (let ((has-subtask)
            (subtree-end (save-excursion (org-end-of-subtree t)))
            (is-a-task (member (nth 2 (org-heading-components)) org-todo-keywords-1)))
        (save-excursion
          (forward-line 1)
          (while (and (not has-subtask)
                      (< (point) subtree-end)
                      (re-search-forward "^\*+ " subtree-end t))
            (when (member (org-get-todo-state) org-todo-keywords-1)
              (setq has-subtask t))))
        (and is-a-task has-subtask))))

  (setq org-agenda-clock-consistency-checks
        (quote (:max-duration "4:00"
                :min-duration 0
                :max-gap 0
                :gap-ok-around ("4:00"))))

  ;;
  ;;Archiving
  ;;

  (setq org-archive-mark-done nil)
  (setq org-archive-location "%s_archive::* Archived Tasks")

  ) ;; after! org

;;
;; Custom Agenda Views
;;
(use-package! org-super-agenda
  :after org-agenda
  :init
  (setq org-agenda-skip-scheduled-if-done t
        org-agenda-skip-deadline-if-done t
        org-agenda-include-deadlines t
        org-agenda-block-separator nil
        org-agenda-compact-blocks t
        org-agenda-start-day nil ;; i.e. today
        org-agenda-span 1
        org-agenda-start-with-log-mode t
        org-super-agenda-header-map (make-sparse-keymap)
        org-agenda-start-on-weekday nil)
  (setq org-agenda-custom-commands
        '(
          (" " "Super View"
           ((agenda "" ((org-agenda-span 'day)
                        (org-super-agenda-groups
                         '((:name "Done Today"
                            :and (:regexp "State \"DONE\""
                                  :log t))
                           (:name "Clocked Today"
                            :log t)
                           ;; (:habit t)
                           (:name "Schedule"
                            :time-grid t
                            :scheduled today
                            :order 0)
                           (:name "Due today"
                            :deadline today)
                           (:name "Overdue"
                            :deadline past)
                           (:name "Due soon"
                            :deadline future)
                           (:name "Unimportant"
                            :todo ("SOMEDAY" "MAYBE" "CHECK" "TO-READ" "TO-WATCH" "HOLD")
                            :order 100)
                           (:habit t
                            :order 2)
                           (:discard (:anything t))
                           ))))
            (todo "" ((org-agenda-overriding-header "")
                      (org-super-agenda-groups
                       '((:discard (:habit t))
                         (:name "WAITING"
                          :todo "WAITING"
                          :order 3)
                         (:name "NEXT"
                          :todo "NEXT"
                          :order 3)
                         (:name "REFILE"
                          :file-path "refile.*\\.org"
                          :order 0)
                         (:discard (:anything t))
                         ))))
            ))
          ("i" "Todo View"
           ((todo "" ((org-agenda-overriding-header "")
                      (org-super-agenda-groups
                       '((:name "REFILE"
                          :file-path "refile.*"
                          :order 0)
                         (:auto-category t
                          :order 9)
                         (:discard (:anything t))
                         ))))))
          ("n" "Notes View"
           ((tags "NOTE"
                  ((org-super-agenda-groups
                    '((:name "NOTES"
                       :tag "NOTE"
                       :order 0)
                      (:discard (:anything t))
                      ))))))
          )
        )
  :config
  (org-super-agenda-mode)
  )

(use-package! avy
  :init
  (setq avy-keys '(104 116 110 115 98 109 119 118 122)) ;; htnsbmwvz
  )

(defun ascii-table ()
  "Display basic ASCII table (0 thru 128)."
  (interactive)
  (switch-to-buffer "*ASCII*")
  (erase-buffer)
  (setq buffer-read-only nil)        ;; Not need to edit the content, just read mode (added)
  (local-set-key "q" 'bury-buffer)   ;; Nice to have the option to bury the buffer (added)
  (save-excursion (let ((i -1))
                    (insert "ASCII characters 0 thru 127.\n\n")
                    (insert " Hex  Dec  Char|  Hex  Dec  Char|  Hex  Dec  Char|  Hex  Dec  Char\n")
                    (while (< i 31)
                      (insert (format "%4x %4d %4s | %4x %4d %4s | %4x %4d %4s | %4x %4d %4s\n"
                                      (setq i (+ 1  i)) i (single-key-description i)
                                      (setq i (+ 32 i)) i (single-key-description i)
                                      (setq i (+ 32 i)) i (single-key-description i)
                                      (setq i (+ 32 i)) i (single-key-description i)))
                      (setq i (- i 96))))))

;; Hex  Dec  Char|  Hex  Dec  Char|  Hex  Dec  Char|  Hex  Dec  Char
;;   0    0  C-@ |   20   32  SPC |   40   64    @ |   60   96    `
;;   1    1  C-a |   21   33    ! |   41   65    A |   61   97    a
;;   2    2  C-b |   22   34    " |   42   66    B |   62   98    b
;;   3    3  C-c |   23   35    # |   43   67    C |   63   99    c
;;   4    4  C-d |   24   36    $ |   44   68    D |   64  100    d
;;   5    5  C-e |   25   37    % |   45   69    E |   65  101    e
;;   6    6  C-f |   26   38    & |   46   70    F |   66  102    f
;;   7    7  C-g |   27   39    ' |   47   71    G |   67  103    g
;;   8    8  C-h |   28   40    ( |   48   72    H |   68  104    h
;;   9    9  TAB |   29   41    ) |   49   73    I |   69  105    i
;;   a   10  C-j |   2a   42    * |   4a   74    J |   6a  106    j
;;   b   11  C-k |   2b   43    + |   4b   75    K |   6b  107    k
;;   c   12  C-l |   2c   44    , |   4c   76    L |   6c  108    l
;;   d   13  RET |   2d   45    - |   4d   77    M |   6d  109    m
;;   e   14  C-n |   2e   46    . |   4e   78    N |   6e  110    n
;;   f   15  C-o |   2f   47    / |   4f   79    O |   6f  111    o
;;  10   16  C-p |   30   48    0 |   50   80    P |   70  112    p
;;  11   17  C-q |   31   49    1 |   51   81    Q |   71  113    q
;;  12   18  C-r |   32   50    2 |   52   82    R |   72  114    r
;;  13   19  C-s |   33   51    3 |   53   83    S |   73  115    s
;;  14   20  C-t |   34   52    4 |   54   84    T |   74  116    t
;;  15   21  C-u |   35   53    5 |   55   85    U |   75  117    u
;;  16   22  C-v |   36   54    6 |   56   86    V |   76  118    v
;;  17   23  C-w |   37   55    7 |   57   87    W |   77  119    w
;;  18   24  C-x |   38   56    8 |   58   88    X |   78  120    x
;;  19   25  C-y |   39   57    9 |   59   89    Y |   79  121    y
;;  1a   26  C-z |   3a   58    : |   5a   90    Z |   7a  122    z
;;  1b   27  ESC |   3b   59    ; |   5b   91    [ |   7b  123    {
;;  1c   28  C-\ |   3c   60    < |   5c   92    \ |   7c  124    |
;;  1d   29  C-] |   3d   61    = |   5d   93    ] |   7d  125    }
;;  1e   30  C-^ |   3e   62    > |   5e   94    ^ |   7e  126    ~
;;  1f   31  C-_ |   3f   63    ? |   5f   95    _ |   7f  127  DEL
